package com.example.android_mood_enhancer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.app.Activity
import android.content.Intent
import android.widget.ImageView
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        var btn_start = findViewById<Button>(R.id.btn_start)

        btn_start.setOnClickListener {
            val intent = Intent(this, Photo_Activity::class.java)
            startActivity(intent)
            finish()
        }

    }

}