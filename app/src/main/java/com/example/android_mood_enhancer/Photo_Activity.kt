package com.example.android_mood_enhancer

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy.Builder
import android.provider.MediaStore
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.android_mood_enhancer.fer.FerModel
import com.example.android_mood_enhancer.fer.FerViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.ByteArrayOutputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL


class Photo_Activity : AppCompatActivity() {
    lateinit var imageView: ImageView
    lateinit var buttonTake: Button
    lateinit var buttonVideo: Button
    var textView: Int = R.id.emotion_display
    private val REQUEST_IMAGE_CAPTURE = 100
    var videoURL : String =""
    override fun onCreate(savedInstanceState: Bundle?) {
        val policy: StrictMode.ThreadPolicy = Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        FerModel.load(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageView = findViewById(R.id.image_save)
        buttonTake = findViewById<Button>(R.id.btn_go)
        buttonVideo = findViewById<Button>(R.id.btn_vid)


        buttonTake.setOnClickListener {

            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

            try{
                startActivityForResult(takePictureIntent,REQUEST_IMAGE_CAPTURE)
            }catch (e: ActivityNotFoundException){
                Toast.makeText(this, "ERROR"+e.localizedMessage, Toast.LENGTH_SHORT).show()
            }

        }
        buttonVideo.setOnClickListener {
            // on below line we are opening our activity for playing a video
            val intentGoToVideo = Intent(this@Photo_Activity, PlayerActivity::class.java)
            intentGoToVideo.putExtra("url", videoURL)
            startActivity(intentGoToVideo)
            finish()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            val imageBitmap = data?.extras?.get("data") as Bitmap
            val outputStream = ByteArrayOutputStream()
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
            imageView.setImageBitmap(imageBitmap)
            val viewModel = ViewModelProvider.NewInstanceFactory().create(FerViewModel::class.java)
            val emotions = viewModel.getEmotionMap(imageBitmap)
            var max = emotions.maxBy { it.value }
            emotion_display.text = max.key
            var urlParams = "anger=${String.format("%.3f",emotions["anger"])}&" +
                    "disgust=${String.format("%.3f",emotions["disgust"])}&" +
                    "fear=${String.format("%.3f",emotions["fear"])}&" +
                    "joy=${String.format("%.3f",emotions["happy"])}&" +
                    "neutral=${String.format("%.3f",emotions["neutral"])}&" +
                    "sad=${String.format("%.3f",emotions["sad"])}&" +
                    "surprise=${String.format("%.3f",emotions["surprised"])}"
            val url = URL(" http://127.0.0.1:8000/video?${urlParams}")
            val conn = url.openConnection() as HttpURLConnection
            conn.requestMethod = "GET"
            BufferedReader(InputStreamReader(conn.inputStream)).use { br ->
                var line: String?
                while (br.readLine().also { line = it } != null) {
                    videoURL += line
                    System.out.println(line)
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

}