package com.example.android_mood_enhancer.fer;

import android.graphics.Bitmap
import androidx.lifecycle.ViewModel

/**
 * Once it receives face pictures, it runs the model and returns prediction emotion labels
 */
class FerViewModel : ViewModel() {

    fun getEmotionMap(faceImage: Bitmap): Map<String, Double> {
        val emotionScores = FerModel.classify(faceImage).toMutableList()
        val emotionLabels = FerModel.getLabels()
        emotionScores[5] += emotionScores[7]
        emotionScores.removeAt(7)
        if(emotionLabels.size == 8) {
            emotionLabels.removeAt(7)
        }
        return emotionLabels.zip(emotionScores).toMap()
    }
}